# tinymce

#### 介绍
tinymce：富文本编辑器

#### 使用说明

- 添加如下的 `layui` 表单行

```html
<div class="layui-form-item">
  <!-- 左侧的 label -->
  <label class="layui-form-label">文章内容</label>
  <!-- 为富文本编辑器外部的容器设置高度 -->
  <div class="layui-input-block" style="height: 400px;">
    <!-- 重要：将来这个 textarea 会被初始化为富文本编辑器 -->
    <textarea name="content"></textarea>
  </div>
</div>
```

- 导入富文本必须的 `script` 脚本

```html
<!-- 富文本 -->
<script src="/assets/lib/tinymce/tinymce.min.js"></script>
<script src="/assets/lib/tinymce/tinymce_setup.js"></script>
```

- 调用 `initEditor()` 方法，初始化富文本编辑器

```js
// 初始化富文本编辑器
initEditor()
```
